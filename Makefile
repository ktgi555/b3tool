include Makefile.inc

all install uninstall:
	$(MAKE) $@ -C mmvm
	$(MAKE) $@ -C distrib

depend:
	$(MAKE) $@ -C mmvm

clean:
	$(MAKE) $@ -C mmvm
	$(MAKE) $@ -C tools
	$(MAKE) $@ -C tests
	$(MAKE) $@ -C Ack-5.5
	$(MAKE) $@ -C 8086v6
	$(MAKE) $@ -C libc
	$(MAKE) $@ -C trans
